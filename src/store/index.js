import { configureStore } from "@reduxjs/toolkit";
import { postsSlice } from "../views/posts/store";

const store = configureStore({
  reducer: {
    posts: postsSlice.reducer,
  },
});

export default store
