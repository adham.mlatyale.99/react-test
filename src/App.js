import PostsList from "./views/posts/pages/PostsList";
import { createClient, Provider } from "urql";
import store from "./store";
import { Provider as ReactReduxProvider } from "react-redux";

const client = createClient({
  url: "https://graphqlzero.almansi.me/api",
});
function App() {
  return (
    <Provider value={client}>
      <ReactReduxProvider store={store}>
        <PostsList />
      </ReactReduxProvider>
    </Provider>
  );
}

export default App;
