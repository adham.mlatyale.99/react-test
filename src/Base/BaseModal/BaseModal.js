import classes from "./BaseModal.module.css";
import ReactDOM from "react-dom";
import React from "react";
const BaseModal = (props) => {
  return (
    <React.Fragment>
      {props.modalState && (
        <React.Fragment>
          {ReactDOM.createPortal(
            <div className={classes.backdrop} onClick={props.onClose}></div>,
            document.getElementById("backdrop-root")
          )}
          {ReactDOM.createPortal(
            <div className={classes.modal}>
              <h1 className={classes["modal-title"]}>{props.title}</h1>
              {props.children}
            </div>,
            document.getElementById("modal-root")
          )}
        </React.Fragment>
      )}
    </React.Fragment>
  );
};

export default BaseModal;
