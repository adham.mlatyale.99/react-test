import "./BaseSpinner.css";

const BaseSpinner = () => {
  return <div className="lds-hourglass"></div>;
};

export default BaseSpinner;
