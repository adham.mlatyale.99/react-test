import "./BaseTable.css";
import BaseSpinner from "../BaseSpinner/BaseSpinner";
import BaseButton from "../BaseButton/BaseButton";

const BaseTable = (props) => {
  const renderedContent = props.loading ? (
    <div className="base-table__loading-wrapper">
      <BaseSpinner />
    </div>
  ) : (
    <div>
      <BaseButton onClick={props.onRefresh}>Refresh</BaseButton>
      <table>
        <thead>
          <tr>
            {props.tableColumns.map((colLabel, key) => {
              return <th key={key}>{colLabel}</th>;
            })}
          </tr>
        </thead>
        <tbody>
          {props?.data.map((data, key) => {
            return (
              <tr key={key}>
                {props.tableColumns.map((el, ix) => {
                  return <td key={ix}>{data[el]}</td>;
                })}
              </tr>
            );
          })}
        </tbody>
      </table>
    </div>
  );
  return renderedContent;
};

export default BaseTable;
