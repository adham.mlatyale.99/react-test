import classes from "./BaseInput.module.css";
const BaseInput = (props) => {
  return (
    <div>
      <label className={classes.label}>{props.label}</label>
      <input
        className={classes.input}
        type={props.type || "text"}
        value={props.value}
        onChange={props.onChange}
      ></input>
    </div>
  );
};

export default BaseInput;
