import classes from "./BaseButton.module.css";

const BaseButton = (props) => {
  return (
    <button
      disabled={props.loading}
      className={classes.button}
      onClick={props.onClick}
    >
      {props.loading ? "Loading.." : props.children}
    </button>
  );
};

export default BaseButton;
