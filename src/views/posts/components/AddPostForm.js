import { useState } from "react";
import { useMutation } from "urql";
import BaseButton from "../../../Base/BaseButton/BaseButton";
import BaseInput from "../../../Base/BaseInput/BaseInput";
import * as yup from "yup";

let schema = yup.object().shape({
  title: yup.string().required(),
  body: yup.string().required(),
});

const addPostQuery = `
mutation (
    $input: CreatePostInput!
  ) {
    createPost(input: $input) {
      id
      title
      body
    }
  }
`;
const AddPostForm = (props) => {
  const [body, setBody] = useState("");
  const [title, setTitle] = useState("");
  const [{ fetching }, createPost] = useMutation(addPostQuery);
  const [error, setError] = useState(false);

  const submit = () => {
    schema
      .validate({
        title,
        body,
      })
      .then((res) => {
        setError("");
        createPost({
          input: {
            body,
            title,
          },
        }).then(res => {
            props.onFormAdded(res.data.createPost);
        });
      })
      .catch((err) => {
        setError(err.errors[0]);
      });
  };
  return (
    <>
      <div
        style={{
          display: "flex",
          justifyContent: "space-around",
        }}
      >
        <BaseInput
          label="Title"
          value={title}
          onChange={(e) => setTitle(e.target.value)}
        />
        <BaseInput
          label="Body"
          value={body}
          onChange={(e) => setBody(e.target.value)}
        />
      </div>
      <div
        style={{
          display: "flex",
          justifyContent: "center",
          flexDirection: "column",
          alignItems: "center",
        }}
      >
        <BaseButton loading={fetching} onClick={submit}>
          Submit
        </BaseButton>
        <small
          style={{
            color: "red",
          }}
        >
          {error}
        </small>
      </div>
    </>
  );
};

export default AddPostForm;
