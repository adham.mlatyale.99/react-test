import { createSlice } from "@reduxjs/toolkit";

const initialState = {
  posts: [],
  fetchingPosts: true,
};
export const postsSlice = createSlice({
  name: "posts",
  initialState,
  reducers: {
    setPosts(state, action) {
      state.posts = action.payload.newPosts;
    },
    setPostsLoading(state, action) {
      state.fetchingPosts = action.payload.status;
    },
  },
});

export const postsActions = postsSlice.actions;
