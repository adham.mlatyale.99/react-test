import { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useQuery } from "urql";
import BaseButton from "../../../Base/BaseButton/BaseButton";
import BaseModal from "../../../Base/BaseModal/BaseModal";
import BaseTable from "../../../Base/BaseTable/BaseTable";
import AddPostForm from "../components/AddPostForm";
import { postsActions } from "../store";

const PostsQuery = `
query ( $options: PageQueryOptions) {
    posts(options: $options) {
      data {
        id
        title
      }
      meta {
        totalCount
      }
    }
  }
`;
const tableColumns = ["id", "title"];
const PostsList = () => {
  const posts = useSelector((state) => state.posts.posts);
  const loadingPosts = useSelector((state) => state.posts.fetchingPosts);
  const [addPostModalState, setAddPostModalState] = useState(false);
  const modalChangeHandler = (state) => setAddPostModalState(state);

  const dispatch = useDispatch();
  const [{ fetching, data }, reexcuteQuery] = useQuery({
    query: PostsQuery,
    variables: {
      options: {
        paginate: {
          page: 1,
          limit: 5,
        },
      },
    },
    requestPolicy: 'network-only'
  });
  useEffect(() => {
    dispatch(
      postsActions.setPostsLoading({
        status: fetching,
      })
    );
    if (!fetching && data) {
      dispatch(
        postsActions.setPosts({
          newPosts: data.posts.data,
        })
      );
    }
  }, [fetching, data, dispatch]);
  const formAddedHandler = (data) => {
    dispatch(
      postsActions.setPosts({
        newPosts: [...posts, data],
      })
    );
    modalChangeHandler(false)
  };
  return (
    <div>
      <BaseModal
        title="Add new post"
        modalState={addPostModalState}
        onClose={() => modalChangeHandler(false)}
      >
        <AddPostForm onFormAdded={formAddedHandler} />
      </BaseModal>
      <BaseButton onClick={() => modalChangeHandler(true)}>
        Add New Post
      </BaseButton>
      <BaseTable
        data={posts}
        tableColumns={tableColumns}
        loading={loadingPosts}
        onRefresh={reexcuteQuery}
      />
    </div>
  );
};

export default PostsList;
